'use strict';

// The name of the package in order to give the Antora logger a useful name
const { name: packageName } = require('../package.json');

const fs = require('fs');
const https = require('https');

const version_adoc = 'version.adoc';
const version_pdf_adoc = 'version-pdf.adoc';
const version_adoc_path = 'modules/ROOT/pages/' + version_adoc;

var logger;

const {gitDescribe, gitDescribeSync} = require('git-describe');
const { execSync } = require('child_process');

function executeGitCommand(command) {
  return execSync(command)
    .toString('utf8')
    .replace(/[\n\r\s]+$/, '');
}

var git_info;
function getGitInfo() {
    if (git_info === undefined) {
        git_info = gitDescribeSync({
            match: null
        });
    }
    return git_info;
}

var derived_version;

function getDerivedVersion() {
    if (derived_version === undefined) {
        // derive version by using tag if distance is 0 or otherwise branchname
        // NOTE: does not work on detached HEAD
        // var branch_name = executeGitCommand('git rev-parse --abbrev-ref HEAD');
        var branch_name = executeGitCommand("git branch --remote --verbose --no-abbrev --contains | sed -rne 's/^[^\\/]*\\/([^\\ ]+).*$/\\1/p'");
        derived_version = branch_name;
        if (getGitInfo().distance == 0) {
            derived_version = getGitInfo().tag;
        }
        if ((derived_version === undefined) || (derived_version === "HEAD")){
            logger.info("branch_name: " + branch_name);
            logger.info("git_info: " + getGitInfo());
            logger.fatal("Derived version undefined");
            throw new Error;
        }
        logger.info("Derived version: " + derived_version);
    }
    return derived_version;
}


module.exports.register = function ({ playbook, config }) {

    logger = this.require('@antora/logger').get(packageName);
    logger.info("register begin");

    // logger.info(playbook);
    // logger.info(config);

    // Modify <branch> in the extensions config
    for (const source of config.data.src) {
        for (let i = 0; i < source.versions.length; i++) {
            if (source.versions[i] == "<branch>") {
                logger.info("Config: changing original version for " + source.component + " from " + source.versions[i] + " to " + getDerivedVersion());
                source.versions[i] = getDerivedVersion();
            }
        }
    }

    // logger.info(config);

    this.on('contentAggregated', function ({ playbook, siteAsciiDocConfig, siteCatalog, contentAggregate }) {
        logger.info("contentAggregated begin");
        logger.info("contentAggregated len: " + contentAggregate.length);
        for (const content of contentAggregate) {
            logger.info("Handling: " + content.name + " " + content.version);
            if (content.version == "<branch>") {
                logger.info("Content: changing version for " + content.name + " from <branch> to " + getDerivedVersion());
                content.version = derived_version;
            }
        }
        logger.info("contentAggregated end");
    })

    this.on('contentClassified', async ({ playbook, siteAsciiDocConfig, siteCatalog, uiCatalog, contentCatalog }) => {
        logger.info("contentClassified begin");

        // for (const component of contentCatalog.getComponents()) {
        //     logger.info(component);
        //     logger.info(component.latest);
        //     component.latest.displayVersion = "1.2";
        //     component.latest.version = "1.2";
        // }

        // logger.info(contentCatalog.getComponents());

        // FIXME --output-dir will set output.dir, otherwise defaults
        var output_dir = 'build/site';
        // var output = this.getVariables().playbook.output;
        // if (output.clean) {
        //     logger.info("Here");
        //     output_dir = output.dir;
        // }
        logger.info("Output dir: " + output_dir);

        for (const source of config.data.src) {
            logger.info("Version files for: " + source.component);
            for (var version of source.versions) {
                logger.info("version " + version);
                var contents, pdf_contents;
                if (source.url) {
                    // Update a multi-version document
                    // example: https://otp-data.web.cern.ch/pdfs/otp-user-manual-1.x-version.adoc
                    var url = source.url + "/" + source.component + '-' + version + "-version.adoc";
                    logger.info("downloading " + url);
                    contents = await download(url);
                    pdf_contents = contents;
                } else {
                    // Create a single version document
                    contents = Buffer.from("Version: " + getGitInfo().raw);
                    pdf_contents = Buffer.from("v" + getGitInfo().raw);
                }

                logger.info("Contents version.adoc: " + contents);
                logger.info("Contents version-pdf.adoc: " + pdf_contents);

                try {
                    contentCatalog.addFile({
                        contents: contents,
                        path: version_adoc_path,
                        src: {
                            path: version_adoc_path,
                            component: source.component,
                            version: version,
                            module: 'ROOT',
                            family: 'page',
                            relative: version_adoc,
                        },
                    });
                    logger.info("Added virtual file " + version_adoc_path + " for " + source.component + " under version " + version);

                    if (!source.url) {
                        // also write to real file to upload
                        var path = output_dir + "/" + source.component + "/" + version + "/" + version_adoc;
                        const folder_path = path.substring(0, path.lastIndexOf('/'));

                        if (!fs.existsSync(folder_path)) {
                            fs.mkdirSync(folder_path, { recursive: true });
                        }
                        fs.writeFileSync(path, contents);
                        logger.info("successfully wrote: '" + path + "'");

                        // and write pdf file for make-pdf
                        var pdf_path = "modules/ROOT/pages/" + version_pdf_adoc;
                        const pdf_folder_path = pdf_path.substring(0, pdf_path.lastIndexOf('/'));

                        if (!fs.existsSync(pdf_folder_path)) {
                            fs.mkdirSync(pdf_folder_path, { recursive: true });
                        }
                        fs.writeFileSync(pdf_path, pdf_contents);
                        logger.info("successfully wrote: '" + pdf_path + "'");
                    }
                } catch (error) {
                    logger.info("contentClassified error");
                    logger.fatal(error);
                    throw new Error;
                }
            }
        }

        // logger.debug(contentCatalog.getComponents());
        // logger.debug(contentCatalog.getFiles());
        logger.info("contentClassified end");
    })

    logger.info("register end");
}

function download(url) {
    return new Promise(resolve => {
        https.get(url, (res) => {
            if (res.statusCode != 200) {
                logger.fatal("Cannot download " + url + " StatusCode: " + res.statusCode + ": " + res.statusMessage);
                throw new Error;
            }
            const data = [];
            res.on('data', (chunk) => {
                data.push(chunk);
            }).on('end', () => {
                let buffer = Buffer.concat(data);
                resolve(buffer);
            });
        }).on('error', (err) => {
            logger.error('download error:', err);
        });
    });
}
