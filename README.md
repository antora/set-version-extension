NOTE: in antora.yml if you use 'version: true' it will only set a branch name, not a tag. It is therefore not very useful.

NOTE: the single version document uploads <manual>-<version>-version.adoc, and <manual>-<version>.pdf to PDFS.

NOTE: the multi version document downloads a number of <manual>-<version>-version.adoc and and <manual>-<version>.pdf files from PDFS

NOTE: the make-html.sh sets the correct version numbers in antora.yml and antora-playbook.yml. These files are committed, and need to be consistent for the multi version document to pick them up. The multi document converter CANNOT and does NOT run make-html.

NOTE: we tried to set the version numbers in the extension itself, by replacing "<branch>" with the correct version number, which seems to work for a single version document, but it does NOT work for a multi-version document. The multi-version document always looks for whatever was set in the checked in antora.yml. In fact it seems that a map is used to store the versions you want in the multi-version document, but as key the version in the antora.yml (in git) is used. Thus 1.1 and 1.x may both have "<branch>" and thus overwrite eachother. The list of versions to handle here is 1 (branch) and not 2 (1.1 and 1.x: as it should be).

NOTE: there seems to be still some descrepancy when asked the multi-version playbook does NOT ask for a certain version but that version is available. Just specify it, but do not include it in the non-extension part of the playbook.


To use this and other extensions (pdf) do the following:

- add submodules under antora/<submodule> using ```git submodule add ../../antora/set-version-extension.git antora/set-version-extension```
- copy antora.yml to antora.in.yml and set version to "<branch>"
- copy antora-playbook.yml to antora-playbook.in.yml and add something along the following:
```
antora:
  extensions:
    - require: "@antora/lunr-extension"
    - require: ./antora/check-config-extension/lib/index.js
    - require: ./antora/set-version-extension/lib/index.js
      data:
        src:
        - component: 'otp-user-manual'
          versions: ["<branch>"]
```
- add the antora.in.yml antora-playbook.in.yml files to git
- use a make-html.sh file like the following:
```
#!/bin/sh
set -e

if [ $# -ge 1 ]; then
  BRANCH=${1}
  shift
else
  BRANCH=`git rev-parse --abbrev-ref HEAD`
fi

#
# Update the antora, antora-playbook
#
sed -e "s/<branch>/${BRANCH}/g" antora.in.yml > antora.yml
sed -e "s/<branch>/${BRANCH}/g" antora-playbook.in.yml > antora-playbook.yml
antora antora-playbook.yml --stacktrace --log-level debug --log-failure-level error
find build
```

- remove any  file from module/ROOT/pages/version.adoc (it will be generated or downloaded)
- in .gitlab-ci you need to
  - build: make-html, which uploads the version.adoc and version-pdf.adoc files
  - pdf-build: update the pdf (optional)
  - trigger: trigger multi-version docs
- So add:
  - ENV vars for the submodules to work in gitlab-ci
  ```
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: normal
  GIT_DEPTH: 0
  ```
  - add npm modules
  ```
  npm install @antora/lunr-extension @antora/site-generator tar-stream fs-extra git-describe
  ```
  - add a deploy-version (set username and password) phase
  ```
  deploy-version:
  stage: deploy
  needs: ["build"]
  script:
  - echo "adding version file to ${CI_COMMIT_REF_NAME}"
  - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
  - cat ${MANUAL}/version.adoc
  - EOS_MGM_URL=${EOSPROJECT} eos cp -r -n ${MANUAL}/version.adoc ${PDFS}/otp-user-manual-${CI_COMMIT_REF_NAME}-version.adoc
  ```
  - disable copy and tag (unless you want a single version manual published [not tested])
  - add a trigger for the multi-version document
  ```
  otp-docs-alpha:
  stage: trigger
  needs: ["deploy-version"]
  trigger:
      project: atlas-otp/otp-docs
      branch: master
      strategy: depend
      forward:
      yaml_variables: false
  allow_failure: true
  ```
- add make-tag.sh
```
#!/bin/sh
set -e

if [ $# -ge 1 ]; then
  TAG=${1}
  shift
else
  echo "Usage: make-tag.sh <tag>"
  exit 1
fi

# Check if tag exists
if [ $(git tag -l "${TAG}") ]; then
    echo "Tag already exists"
    exit 1
fi

# Create the tag and commit
./make-html.sh ${TAG}
./make-pdf.sh ${TAG}
git commit -a -m "Added ${TAG}"
git tag ${TAG}

# Put branch back and commit
./make-html.sh
./make-pdf.sh
git commit -a -m "Set back branch"

# Show log
git log -n 5

# Push all
git push
echo "Waiting 15 seconds..."
sleep 15
git push --tags
```
